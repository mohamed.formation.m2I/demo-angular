import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { NonNullableFormBuilder } from '@angular/forms';

@Directive({
  selector: '[cpLoop]',
})
export class LoopDirective {
  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef
  ) {}

  @Input('cpLoop') set loop(nombre: number) {
    for (var i = 0; i < nombre; i++) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    }
  }
}
