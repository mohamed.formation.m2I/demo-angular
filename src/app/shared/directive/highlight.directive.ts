import {
  Directive,
  ElementRef,
  HostBinding,
  HostListener,
  Renderer2,
} from '@angular/core';
import { HotObservable } from 'rxjs/internal/testing/HotObservable';

@Directive({
  selector: '[appHighlight]',
})
export class HighlightDirective {

  @HostBinding('style.border') border: string;

  constructor(private e: ElementRef, private render: Renderer2) {
    this.changeColor('yellow');
  }

  changeColor(color: string) {
    // this.e.nativeElement.style.backgroundColor = color;
    this.render.setStyle(this.e.nativeElement, 'backgroundColor', color);
  }

  @HostListener('mouseover') onMouseOver() {
    this.changeColor('blue');
    this.border = "5px solid red";
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.changeColor('green');
    this.border = "10px dashed black";
  }

  @HostListener('click') onClick() {
    alert('Tu as cliqué sur moi !!');
  }
}
