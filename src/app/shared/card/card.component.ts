import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  @Input()
  inputFromParent: string | undefined;

  @Output()
  outPutFromChild: EventEmitter<string> = new EventEmitter();

  outPutText: string = 'Adopte moi !!';

  constructor() {}

  sendDataToParent() {
    this.outPutFromChild.emit(this.outPutText);
  }

  ngOnInit(): void {}
}
