import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navside',
  templateUrl: './navside.component.html',
  styleUrls: ['./navside.component.scss'],
})
export class NavsideComponent implements OnInit {
  id: number = 13;
  constructor(private route: Router) {}

  ngOnInit(): void {}

  navigation() {
    this.route.navigate(['/page4']);
  }
}
