import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderService } from 'src/app/service/header.service';
import { LoginService } from 'src/app/service/login.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  titre: string = ' ';
  constructor(
    private headerService: HeaderService,
    private loginService: LoginService,
    private route: Router
  ) {}

  ngOnInit(): void {
    this.headerService.change.subscribe((x) => {
      this.titre = x;
    });
  }

  deconnect() {
    this.loginService.deconnect();
    this.route.navigate(['page1']);
  }
}
