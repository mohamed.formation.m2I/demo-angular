import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErrorComponent } from './component/error/error.component';
import { Page1Component } from './component/page1/page1.component';
import { Page10Component } from './component/page10/page10.component';
import { Page2Component } from './component/page2/page2.component';
import { Page3Component } from './component/page3/page3.component';
import { Page4Component } from './component/page4/page4.component';
import { Page5Component } from './component/page5/page5.component';
import { Page6Component } from './component/page6/page6.component';
import { Page7Component } from './component/page7/page7.component';
import { Page8Component } from './component/page8/page8.component';
import { Page9Component } from './component/page9/page9.component';
import { AuthService as AuthGuard } from './service/auth.service';

const routes: Routes = [
  { path: 'page1', component: Page1Component },
  { path: 'page2', component: Page2Component, canActivate: [AuthGuard] },
  /*  { path: 'page3', component: Page3Component }, */
  { path: 'page4', component: Page4Component, canActivate: [AuthGuard] },
  /*  { path: 'page5', component: Page5Component }, */
  {
    path: 'page6',
    children: [
      { path: 'home', component: Page6Component },
      { path: ':id', component: Page3Component },
      { path: ':id/other', component: Page5Component },
    ],
    canActivate: [AuthGuard],
  },
  { path: 'page7', component: Page7Component, canActivate: [AuthGuard] },
  { path: 'page8', component: Page8Component, canActivate: [AuthGuard] },
  { path: 'page9', component: Page9Component, canActivate: [AuthGuard] },
  { path: 'page10', component: Page10Component, canActivate: [AuthGuard] },
  { path: '', component: Page1Component },
  { path: '**', component: ErrorComponent },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
