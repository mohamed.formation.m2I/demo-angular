import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'startM',
})
export class StartMPipe implements PipeTransform {
  tab: string[] = [];

  transform(value: string[]): string[] {
    value.forEach((x) => {
      if (x.startsWith('M')) {
        this.tab.push(x);
      }
    });

    return this.tab;
  }
}
