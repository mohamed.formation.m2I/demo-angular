import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'acroNyme',
})
export class AcroNymePipe implements PipeTransform {
  val: any;
  transform(value: string): string {
    this.val = value
      .split(' ')
      .map((x) => x[0])
      .join('.')
      .toUpperCase();
    return this.val;
  }
}
