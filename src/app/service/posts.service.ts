import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from '../model/post';

@Injectable({
  providedIn: 'root',
})
export class PostsService {
  url: string = 'http://localhost:3000/posts';

  constructor(private http: HttpClient) {}

  getPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(this.url);
  }

  createPost(post: Post): Observable<any> {
    const httpOptions: { headers: any; observe: any } = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      observe: 'response',
    };
    return this.http.post(this.url, post, httpOptions);
  }

  delete(id: number): Observable<any> {
    const httpOptions: { headers: any; observe: any } = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      observe: 'response',
    };

    return this.http.delete(this.url + '/' + id, httpOptions);
  }

  udpate(post: Post): Observable<any> {
    const httpOptions: { headers: any; observe: any; responseType: any } = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      observe: 'response',
      responseType: 'text',
    };
    return this.http.put(this.url + '/' + post.id, post, httpOptions);
  }

  getPostById(id: number): Observable<Post> {
    return this.http.get<Post>(this.url + '/' + id, {responseType:'json'});
  }


  postImage(file: File): Observable<any> {
    let formData: FormData = new FormData();
    formData.append('uploadFile', file, file.name);
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    let httpOption = { header: headers, reportProgress: true };
    return this.http.post<any>(this.url, formData, httpOption);
  }
}
