import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService implements CanActivate {
  constructor(private serviceLogin: LoginService, private router: Router) {}

  canActivate(): boolean {
    if (this.serviceLogin.isLog()) {
      return true;
    } else {
      this.router.navigate(['page1']);
      return false;
    }
  }
}
