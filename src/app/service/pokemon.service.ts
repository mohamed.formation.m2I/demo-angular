import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { count, filter, flatMap, map, Observable, pluck } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PokemonService {
  url: string = 'http://localhost:3001/pokemons';

  constructor(private http: HttpClient) {}

  // Promise :
  getPokemons() {
    return this.http.get(this.url).toPromise();
  }

  // Observable :
  getNumberPokemonTypeGrass(): Observable<any> {
    return this.http.get<any>(this.url).pipe(
      flatMap((e) => e),
      filter((res: any) => res.type[0] == 'Grass' || res.type[1] == 'Grass'),
      count()
    );
  }

  getPokemonNameWithSpeedUpperThan50() {
    return this.http.get<any>(this.url).pipe(
      flatMap((e) => e),
      filter((poke: any) => poke.base.Speed > 50),
      pluck('name'),
      map((name) => name.english)
    );
  }

  getPokemonNameUnderId10() {
    return this.http.get<any>(this.url).pipe(
      flatMap((e) => e),
      filter((poke: any) => poke.id < 10),
      pluck('name'),
      map((name) => name.french)
    );
  }





}
