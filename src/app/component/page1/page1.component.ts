import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderService } from 'src/app/service/header.service';
import { LoginService } from 'src/app/service/login.service';

@Component({
  selector: 'app-page1',
  templateUrl: './page1.component.html',
  styleUrls: ['./page1.component.scss'],
})
export class Page1Component implements OnInit {
  erreur: boolean = false;

  // header
  titre: string = 'Page 1';

  // Interpolation :

  myName: string = 'Mohamed';
  myAge: number = 23;
  isBeauGoss: boolean = false;

  // property binding

  person: string = 'Marco polo';
  age: number = 45;
  address: any = {
    street: 'rue du Paradis',
    city: '75000 Paris',
  };

  alignement: string = 'right';

  // event Binding :

  mot: string = 'urgent';
  alert: string = 'alert alert-danger';
  isDanger: boolean = true;

  changeMot() {
    this.isDanger = !this.isDanger;
    this.alert = this.isDanger ? 'alert alert-danger' : 'alert alert-success';
    this.mot = this.isDanger ? 'Urgent' : 'Pas urgent';
  }

  modification: string = ' ';

  modifie(valeur: any) {
    console.log(valeur.target.value);
    this.modification = valeur.target.value;
  }

  modificationEncore: number = 0;

  modifieEncore(valeur: any) {
    console.log(valeur);
    this.modificationEncore = valeur.target.value;
  }

  focused() {
    console.log('focus');
  }

  // Two-way Binding
  userName: string = 'Michou';

  constructor(
    private headerService: HeaderService,
    private route: Router,
    private loginService: LoginService
  ) {}

  ngOnInit(): void {
    this.headerService.changeTitre(this.titre);
  }

  password(valeur: any) {
    if (valeur.target.value == 'password') {
      this.erreur = false;
      this.loginService.connect();
      this.route.navigate(['page2']);
    } else {
      this.erreur = true;
    }
  }
}
