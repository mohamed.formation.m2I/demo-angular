import { Component, OnInit } from '@angular/core';
import { PokemonService } from 'src/app/service/pokemon.service';

@Component({
  selector: 'app-page10',
  templateUrl: './page10.component.html',
  styleUrls: ['./page10.component.scss'],
})
export class Page10Component implements OnInit {
  constructor(private pokemonService: PokemonService) {}

  ngOnInit(): void {
    // this.getPokemon();
    // this.getNumberPokemonTypeGrass();
    // this.getPokemonNameWithSpeedUpperThan50();
    this.getPokemonNameIdUnder10()
  }

  getPokemon() {
    this.pokemonService
      .getPokemons()
      .then((data) => {
        console.log(JSON.stringify(data));
      })
      .catch((error) => {
        console.log('Promesse rejeté pour : ' + JSON.stringify(error.message));
      });
  }

  getNumberPokemonTypeGrass() {
    this.pokemonService
      .getNumberPokemonTypeGrass()
      .subscribe((res) => console.log('Le nombre de pokemon Grass : ' + res));
  }

  getPokemonNameWithSpeedUpperThan50() {
    this.pokemonService
      .getPokemonNameWithSpeedUpperThan50()
      .subscribe((res) => console.log(res));
  }

  getPokemonNameIdUnder10() {
    this.pokemonService.getPokemonNameUnderId10().subscribe({
      next: (x) => console.log(x),
      error: (err) =>
        console.log("Il s'agit d'une erreur " + err.message + ' ' + err.status),
      complete: () => console.log('Fin'),
    });
  }
}
