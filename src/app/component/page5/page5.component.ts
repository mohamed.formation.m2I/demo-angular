import { Component, OnDestroy, OnInit } from '@angular/core';
import { HeaderService } from 'src/app/service/header.service';

@Component({
  selector: 'app-page5',
  templateUrl: './page5.component.html',
  styleUrls: ['./page5.component.scss'],
})
export class Page5Component implements OnInit, OnDestroy {
  titre: string = ' le cycle de vie des composants';
  message: string = 'Je suis une phrase !!!';
  constructor(private headerService: HeaderService) {}

  ngOnInit(): void {
    alert('OnInit');
    this.headerService.changeTitre(this.titre);
    setTimeout(() => {
      alert("Le changement, c'est maintenant !!");
      this.message = "Le changement, c'est maintenant !!";
    }, 5000);
  }

  ngOnChanges() {
    alert('OnChange');
  }

  ngAfterContentInit() {
    alert('OnContentInit');
  }

  ngAfterContentChecked() {
    alert('OnContentChecked');
  }

  ngAfterContentChanged() {
    alert('OnContentChanged');
  }

  ngAfterViewInit() {
    alert('AfterViewinit');
  }

  ngAfterViewChecked() {
    alert('AfterViewChecked');
  }

  ngAfterViewChanged() {
    alert('AfterViewChanged');
  }

  ngOnDestroy(): void {
    alert('Ondestroy');
  }
}
