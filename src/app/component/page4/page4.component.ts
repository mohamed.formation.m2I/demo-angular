import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page4',
  templateUrl: './page4.component.html',
  styleUrls: ['./page4.component.scss'],
})
export class Page4Component implements OnInit {
  inputText: string = ' ';
  message: string = ' ';

  constructor() {}

  receiveChildData($event:any) {
    this.message = $event;
  }

  ngOnInit(): void {}
}
