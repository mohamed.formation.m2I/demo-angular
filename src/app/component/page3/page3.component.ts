import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Params } from '@angular/router';
import { ModalComponent } from 'src/app/modal/modal/modal.component';
import { DataService } from 'src/app/service/data.service';
import { HeaderService } from 'src/app/service/header.service';

@Component({
  selector: 'app-page3',
  templateUrl: './page3.component.html',
  styleUrls: ['./page3.component.scss'],
})
export class Page3Component implements OnInit {
  unPrenom: string;
  message: string = 'Salut les amis';
  nombre: number = 0;
  id: number = 0;
  id2: number = 0;
  titre: string = 'Page 3';
  date: Date = new Date();
  nomPrenom: string = 'Mohamed';
  formation: string = 'Hello Les Amis';
  user: {} = ' ';
  tab: any[] = [];
  donne: number = Math.floor(Math.random() * 10) / 1000;
  marque: string = 'anima sana in corpore sano';
  tabString: string[] = [
    'Marco',
    'Paulo',
    'Michel',
    'Paul',
    'Mike',
    'Jack',
    'John',
    'Mohamed',
    'Tom',
    'Mathieu',
  ];

  constructor(
    private dataService: DataService,
    private headerService: HeaderService,
    private activateRoute: ActivatedRoute,
    public dialog: MatDialog
  ) {
    this.id = this.activateRoute.snapshot.params['id'];
    this.nombre = this.activateRoute.snapshot.params['id'];
    this.activateRoute.params.subscribe((param: Params) => {
      this.id2 = param['id'];
    });
    console.log('valeur id : ' + this.id + ' ' + this.id2);
  }

  ngOnInit(): void {
    this.openDialog();
    this.headerService.changeTitre(this.titre);
    this.tab = this.dataService.getData();
    this.user = this.tab[0];
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '250',
      data: { message: this.message, nombre: this.nombre },
    });
  }

  affiche(prenom: HTMLInputElement) {
    this.unPrenom = prenom.value;
  }
}
