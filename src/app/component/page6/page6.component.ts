import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-page6',
  templateUrl: './page6.component.html',
  styleUrls: ['./page6.component.scss'],
})
export class Page6Component implements OnInit {
  constructor(private route: Router) {}

  ngOnInit(): void {}

  navigationed(valeur: any) {
    this.route.navigate(['/page6', valeur.target.value]);
  }
}
