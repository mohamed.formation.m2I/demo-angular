import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  MaxLengthValidator,
  Validators,
} from '@angular/forms';
import { User } from 'src/app/model/user';

@Component({
  selector: 'app-page7',
  templateUrl: './page7.component.html',
  styleUrls: ['./page7.component.scss'],
})
export class Page7Component implements OnInit {
  myGroup: FormGroup;
  user: User;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.myGroup = this.fb.group({
      firstname: this.fb.control('', Validators.required),
      lastname: this.fb.control('', Validators.required),
      email: this.fb.control('', [
        Validators.required,
        Validators.pattern('^([a-z]*)@([a-z]{2,10}).(fr|com)$'),
      ]),
      password: this.fb.control('', [
        Validators.required,
        Validators.pattern(
          '^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$'
        ),
      ]),
      street: this.fb.control('', Validators.required),
      city: this.fb.control('', Validators.required),
      state: this.fb.control('', Validators.required),
      zip: this.fb.control('', [
        Validators.required,
        Validators.compose([Validators.minLength(5), Validators.maxLength(5)]),
      ]),
      tel: this.fb.control('', [
        Validators.required,
        Validators.pattern('^(+33|0|0033)[1-9][0-9]{8}$'),
      ]),
    });
  }

  get firstname() {
    return this.myGroup.get('firstname');
  }

  get lastname() {
    return this.myGroup.get('lastname');
  }

  get password() {
    return this.myGroup.get('password');
  }
  get email() {
    return this.myGroup.get('email');
  }
  get street() {
    return this.myGroup.get('street');
  }
  get city() {
    return this.myGroup.get('city');
  }

  get state() {
    return this.myGroup.get('state');
  }

  get zip() {
    return this.myGroup.get('zip');
  }

  get tel() {
    return this.myGroup.get('tel');
  }

  validation() {
    if (this.myGroup.valid) {
      this.user = this.myGroup.value;
      //this.user.email = this.myGroup.get('email').value;
      console.log(this.user);
    }
  }
}
