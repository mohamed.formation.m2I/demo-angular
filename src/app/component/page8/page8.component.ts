import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Post } from 'src/app/model/post';
import { PostsService } from 'src/app/service/posts.service';

@Component({
  selector: 'app-page8',
  templateUrl: './page8.component.html',
  styleUrls: ['./page8.component.scss'],
})
export class Page8Component implements OnInit {
  liste: Post[] = [];

  form: FormGroup;
  post: Post;

  constructor(private postService: PostsService, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.getPost();

    this.form = this.fb.group({
      id: ['', Validators.required],
      titre: ['', Validators.required],
      description: ['', Validators.required],
      date: ['', Validators.required],
    });
  }

  get id() {
    return this.form.get('id');
  }

  get titre() {
    return this.form.get('titre');
  }

  get description() {
    return this.form.get('description');
  }

  get date() {
    return this.form.get('date');
  }

  getPost() {
    this.postService.getPosts().subscribe((posts) => {
      this.liste = posts;
    });
  }

  validation() {
    if (this.form.valid) {
      this.post = this.form.value;
      if (this.post.id == undefined) {
        this.postService.createPost(this.post).subscribe((response) => {
          console.log(response);
          alert(response.status);
          this.getPost();
        });
      } else {
        this.postService.udpate(this.post).subscribe((response) => {
          alert(response.status);
          this.getPost();
        });
      }
    }
  }

  delete(id: number) {
    this.postService.delete(id).subscribe((response) => {
      alert(response.status);
      this.getPost();
    });
  }

  edit(id: number) {
    this.postService.getPostById(id).subscribe((post) => {
      this.form.get('id').patchValue(post.id);
      this.form.get('titre').patchValue(post.titre);
      this.form.get('description').patchValue(post.description);
      this.form.get('date').patchValue(post.date);
    });
  }

  
}
