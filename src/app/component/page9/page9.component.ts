import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page9',
  templateUrl: './page9.component.html',
  styleUrls: ['./page9.component.scss'],
})
export class Page9Component implements OnInit {

  showcpIf: boolean = false;
  showcpDelay: boolean = false;
  delay: number = 0;
  
  constructor() {}

  ngOnInit(): void {}
}
