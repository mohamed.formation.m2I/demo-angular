import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';
import { HeaderService } from 'src/app/service/header.service';

@Component({
  selector: 'app-page2',
  templateUrl: './page2.component.html',
  styleUrls: ['./page2.component.scss'],
})
export class Page2Component implements OnInit {
  titre: string = 'Page 2';
  isVisible: boolean = false;
  compteur: number = 0;
  isValid: boolean = true;
  isLoggIn: boolean = true;
  person: string = ' ';
  indicateur: number = 0;
  users: any[] = [];
  tab: string[] = ['Marco', 'Paulo', 'Alain', 'Pierre', 'Corentin'];
  valeur: boolean = true;
  alert: string = 'alert alert-info';

  constructor(
    private dataService: DataService,
    private headerService: HeaderService
  ) {
    this.users = dataService.getData();
  }

  changement() {
    this.isVisible = !this.isVisible;
    this.compteur++;
    this.isValid = !this.isValid;
    this.isLoggIn = !this.isLoggIn;
  }

  changeName() {
    if (this.indicateur <= 4) {
      this.person = this.tab[this.indicateur];
    } else {
      this.indicateur = 0;
      this.person = this.tab[this.indicateur];
    }
    this.indicateur++;
  }

  change() {
    this.valeur = !this.valeur;
  }

  ngOnInit(): void {
    this.headerService.changeTitre(this.titre);
    setTimeout(() => {
      this.alert = 'alert alert-danger';
    }, 3000);
  }
}
