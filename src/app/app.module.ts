import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Page1Component } from './component/page1/page1.component';
import { Page2Component } from './component/page2/page2.component';
import { ErrorComponent } from './component/error/error.component';
import { HeaderComponent } from './shared/header/header.component';
import { NavsideComponent } from './shared/navside/navside.component';
import { Page3Component } from './component/page3/page3.component';
import { AcroNymePipe } from './tools/acro-nyme.pipe';
import { StartMPipe } from './tools/start-m.pipe';
import { HeaderService } from './service/header.service';
import { Page4Component } from './component/page4/page4.component';
import { CardComponent } from './shared/card/card.component';
import { Page5Component } from './component/page5/page5.component';
import { Page6Component } from './component/page6/page6.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalComponent } from './modal/modal/modal.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import { Page7Component } from './component/page7/page7.component';
import { Page8Component } from './component/page8/page8.component';
import { HttpClientModule } from '@angular/common/http';
import { Page9Component } from './component/page9/page9.component';
import { HighlightDirective } from './shared/directive/highlight.directive';
import { IfDirective } from './shared/directive/if.directive';
import { LoopDirective } from './shared/directive/loop.directive';
import { DelayDirective } from './shared/directive/delay.directive';
import { Page10Component } from './component/page10/page10.component';

@NgModule({
  declarations: [
    AppComponent,
    Page1Component,
    Page2Component,
    ErrorComponent,
    HeaderComponent,
    NavsideComponent,
    Page3Component,
    AcroNymePipe,
    StartMPipe,
    Page4Component,
    CardComponent,
    Page5Component,
    Page6Component,
    ModalComponent,
    Page7Component,
    Page8Component,
    Page9Component,
    HighlightDirective,
    IfDirective,
    LoopDirective,
    DelayDirective,
    Page10Component,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatDialogModule,
    MatButtonModule,
    MatInputModule,
    HttpClientModule
  ],
  providers: [HeaderService],
  bootstrap: [AppComponent],
})
export class AppModule {}
